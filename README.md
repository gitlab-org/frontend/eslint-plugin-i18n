[![pipeline status](https://gitlab.com/gitlab-org/frontend/eslint-plugin-i18n/badges/master/pipeline.svg)](https://gitlab.com/gitlab-org/frontend/eslint-plugin-i18n/commits/master)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

# eslint-plugin-i18n

Detect and autofix strings which require externalization in Vanilla JS files.

## Installation

You'll first need to install [ESLint](http://eslint.org):

```
$ npm i eslint --save-dev
```

Next, install `@gitlab/eslint-plugin-i18n`:

```
$ npm install @gitlab/eslint-plugin-i18n --save-dev
```

**Note:** If you installed ESLint globally (using the `-g` flag) then you must also install `@gitlab/eslint-plugin-i18n` globally.

## Usage

Add `@gitlab/eslint-plugin-i18n` to the plugins section of your `.eslintrc` configuration file. You can omit the `@gitlab/eslint-plugin-` prefix:

JSON:

```json
{
  "plugins": ["@gitlab/i18n"]
}
```

YAML:

```yaml
plugins:
  - @gitlab/i18n
```

Then configure the rules you want to use under the rules section.

JSON:

```json
{
  "rules": {
    "@gitlab/i18n/no-non-i18n-strings": "error"
  }
}
```

YAML:

```yaml
rules:
  "@gitlab/i18n/no-non-i18n-strings": error
```

## VueJS (.vue files)
`@gitlab/eslint-plugin-i18n` can also detect strings requiring externalization for code within the `<script></script>` tags of a **.vue** file. 

### Editor support
* [Visual studio Code](https://code.visualstudio.com/) + [Vetur](https://vuejs.github.io/vetur/linting-error.html): add `.vue` to your [linting configuration](https://vuejs.github.io/vetur/linting-error.html#linting)
* [Atom](https://atom.io/) + [linter-eslint](https://github.com/AtomLinter/linter-eslint): add `text.html.vue` to your [list of scopes](https://alligator.io/vuejs/vue-eslint-plugin/#editor-configuration)

## Supported Rules

- [no-non-i18n-strings](https://gitlab.com/gitlab-org/frontend/eslint-plugin-i18n/blob/master/docs/rules/no-non-i18n-strings.md)

## Contribution guidelines

Please refer to [gitlab-ce's CONTRIBUTING.md](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md) for details on our guidelines.

