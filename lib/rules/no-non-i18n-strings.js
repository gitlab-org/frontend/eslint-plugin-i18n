/**
 * @fileoverview Detect a string which has been hard coded and requires externalization.
 * @author Brandon Labuschagne
 */
"use strict";

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------

module.exports = {
  meta: {
    docs: {
      description:
        "Detect a string which has been hard coded and requires externalization.",
      category: "i18n",
      recommended: false
    },
    fixable: "code"
  },

  create: function(context) {
    const message = "Should not have non i18n strings";

    const localizeMethods = ["__", "n__", "s__"];

    const htmlProperties = [
      "className",
      "class",
      "style",
      "event",
      "type",
      "font",
      "key",
      "url",
      "borderColor",
      "path",
      "onclick",
      "transition",
      "rootMargin",
      "base64",
      "sprite",
      "href",
      "length",
      "forceFallback"
    ];

    const cssProperties = ["width", "height"];

    const htmlElements = [
      "thead",
      "tr",
      "td",
      "th",
      "sup",
      "sub",
      "kbd",
      "q",
      "samp",
      "var"
    ];

    const objectMethods = [
      "indexOf",
      "includes",
      "lastIndexOf",
      "matches",
      "find",
      "attr",
      "split",
      "exec",
      "one"
    ];

    const htmlCalls = [
      "$",
      "querySelector",
      "querySelectorAll",
      "createEvent",
      "closest",
      "data",
      "find",
      "hasClass",
      "on",
      "getData",
      "off",
      "bind",
      "removeClass",
      "addClass",
      "registerEventListener",
      "addEventListener",
      "removeEventListener"
    ];

    const dateMethods = ["dateFormat"];

    function isFileName(value) {
      return /^(\w+\.?)*\.\w{2,}$/.test(value);
    }

    function isPath(value) {
      return /^([\w-_]*|\.{1,2})[\/\~]+[\w-_\/]*/.test(value);
    }

    function isUrl(value) {
      return (
        /[a-zA-Z0-9]*(:\/\/)+[a-zA-Z0-9\.]*/.test(value) ||
        /(http){1}[s:\/a-zA-Z0-9.]*/.test(value)
      );
    }

    function isPlaceholder(value) {
      return /^{?{\w+}}?$/.test(value);
    }

    function isListOfHtmlElements(value) {
      const splitChar = value.includes(",") ? ", " : " ";
      return value.split(splitChar).every(string => {
        return htmlElements.includes(string);
      });
    }

    function isListOfCssProperties(value) {
      const splitChar = value.includes(",") ? ", " : " ";
      return value.split(splitChar).every(string => {
        return cssProperties.includes(string);
      });
    }

    function isCssSelector(value) {
      return (
        /^,?\s?[\.\#\*\@]\w+.*/.test(value) ||
        /^[a-zA-Z]+[a-zA-Z-]*[\.\#\*]+[a-zA-Z]+[a-zA-Z-]*/.test(value) ||
        /([a-zA-Z][a-zA-Z-]+[a-zA-Z])+\.\w+/.test(value) ||
        /([a-zA-Z]+[a-zA-Z-]*|\*)\[.*\]/.test(value) ||
        /^[a-zA-Z]+[a-zA-Z-]*[\#\*]$/.test(value) ||
        /^(?:[0-9]+[a-z]|[a-z]+[0-9])[a-z0-9]*\s[a-z0-9][a-z0-9]+-[a-z0-9-]*$/.test(
          value
        ) ||
        isListOfCssProperties(value) ||
        isListOfHtmlElements(value)
      );
    }

    function isCssValue(value) {
      return (
        /^\d+(%|rem|em|vh|vw|px)$/.test(value) ||
        /^\(\w+:\s-[a-z0-9-]+\)/.test(value)
      );
    }

    function isMethodCall(value) {
      return /^\w+\(.*\)$/.test(value);
    }

    function isKebabCase(value) {
      return (
        /^([a-zA-Z0-9]+-[a-zA-Z0-9-]*[\s,]*)+$/.test(value) ||
        /^([a-z0-9]+-[a-z0-9-]*)+(\s[a-z0-9-]*)*/.test(value) ||
        /(\s[a-z0-9-]*)*([a-z0-9]+-[a-z0-9-]*)+$/.test(value)
      );
    }

    function isScreamingKebabCase(value) {
      return /^([A-Z]+-[A-Z-]*[A-Z]*[\s,]*)+$/.test(value);
    }

    function containsCamelCase(value) {
      return /[a-z]+[A-Z]{1}/.test(value);
    }

    function containsSnakeCase(value) {
      return /([a-z]+_[a-z_]*[\s,]*)+/.test(value);
    }

    function isScreamingSnakeCase(value) {
      return /^([A-Z]+_[A-Z_]+[A-Z][\s,]*)+$/.test(value);
    }

    function isIndex(value) {
      return /^[\w-]+:[\w-]+$/.test(value);
    }

    function containsAssignment(value) {
      return value.match(/([a-z]*=['|"])/) || value.match(/(\[[a-z]*=)/);
    }

    function isSpecial(value) {
      return (
        /^[\:\%\_\&\#\?\[\$].*[^.]/.test(value) || /.*[\*\+]+$/.test(value)
      );
    }

    function isVariableName(value) {
      return (
        containsCamelCase(value) ||
        containsSnakeCase(value) ||
        isKebabCase(value) ||
        isScreamingKebabCase(value) ||
        isScreamingSnakeCase(value)
      );
    }

    function isHtml(value) {
      return /<("[^"]*"|'[^']*'|[^'">])*>/.test(value);
    }

    function isColonSeperated(value) {
      return /(([a-zA-Z])*:)+([a-zA-Z])+/.test(value);
    }

    function isNonEmptyString(value) {
      return typeof value === "string" && value.trim() !== "";
    }

    function isNumber(value) {
      return /^\d+$/.test(value);
    }

    function isLowerCaseWord(value) {
      return /^[a-z0-9]+$/.test(value);
    }

    function isUpperCaseWord(value) {
      return /^[A-Z]+$/.test(value);
    }

    function isConstantVariableName(value) {
      return isUpperCaseWord(value) || isScreamingSnakeCase(value);
    }

    function hasOneWord(value) {
      return /[a-zA-Z]{1,}\s|[a-zA-Z]{2,}/.test(value);
    }

    function isDateFormat(value) {
      return /^[M|D|Y|m|d|y\s|,]+$/.test(value);
    }

    function isEmptyVariableAssignment(value) {
      return /\w+=["|']{2}/.test(value);
    }

    function isKeyboardShortcut(value) {
      return /([a-z0-9]*\+{1})+([a-z0-9]+)/.test(value);
    }

    function isNonLocalizedStringValue(value) {
      return (
        isNonEmptyString(value) &&
        hasOneWord(value) &&
        !isLowerCaseWord(value) &&
        !isUpperCaseWord(value) &&
        !isNumber(value) &&
        !isPath(value) &&
        !isUrl(value) &&
        !isHtml(value) &&
        !isCssSelector(value) &&
        !isCssValue(value) &&
        !isMethodCall(value) &&
        !isVariableName(value) &&
        !isSpecial(value) &&
        !isFileName(value) &&
        !isPlaceholder(value) &&
        !isIndex(value) &&
        !isDateFormat(value) &&
        !isEmptyVariableAssignment(value) &&
        !isKeyboardShortcut(value) &&
        !isColonSeperated(value) &&
        !containsAssignment(value)
      );
    }

    function isStringLiteral(node) {
      return node.type === "Literal";
    }

    function isTemplateLiteral(node) {
      return node.type === "TemplateLiteral";
    }

    function stripVariables(value) {
      return value ? value.replace(/(\%{[\w-]+})/g, "") : "";
    }

    function getTemplateLiteralString(node) {
      return node.quasis
        .filter(quasi => quasi.type === "TemplateElement")
        .map(quasi => quasi.value.raw)
        .join("");
    }

    function getCallExpressionString(node, callee) {
      if (callee.name === "sprintf") {
        return stripVariables(node.value);
      }
      return node.value;
    }

    function isNonLocalizedString(node) {
      if (node.parent.callee) {
        return isNonLocalizedStringValue(
          getCallExpressionString(node, node.parent.callee)
        );
      }
      if (isTemplateLiteral(node)) {
        return isNonLocalizedStringValue(getTemplateLiteralString(node));
      }
      if (isStringLiteral(node)) {
        return isNonLocalizedStringValue(node.value);
      }
      return false;
    }

    function applyFix(fixer, literal) {
      return [
        fixer.insertTextBefore(literal, "__("),
        fixer.insertTextAfter(literal, ")")
      ];
    }

    function reportAndFix(context, node) {
      context.report({
        node,
        message,
        fix(fixer) {
          return applyFix(fixer, node);
        }
      });
    }

    function isTrueContstant(node, declaration) {
      return (
        node.kind === "const" && isConstantVariableName(declaration.id.name)
      );
    }

    function isKeyList(declaration, expression) {
      return (
        !!declaration.id &&
        expression.type === "ArrayExpression" &&
        ["key", "keys"].includes(declaration.id.name)
      );
    }

    function isMemberExpressionIdentifier(expression) {
      if (expression.type !== "MemberExpression") return false;
      if (!!expression.property.quasis) {
        return /^\w+$/.test(getTemplateLiteralString(expression.property));
      }
      return /^\w+$/.test(expression.property.value);
    }

    function isHtmlProperty(node) {
      if (node.type === "Identifier") {
        return htmlProperties.includes(node.name);
      }
      if (node.type === "MemberExpression") {
        return htmlProperties.includes(node.property.name);
      }
      return false;
    }

    function isLocalizedCall(node) {
      return localizeMethods.includes(node.callee.name);
    }

    function isObjectMethod(node) {
      if (!!node.callee.property) {
        return objectMethods.includes(node.callee.property.name);
      }
      return false;
    }

    function isHtmlCall(node) {
      if (!!node.callee.property) {
        return htmlCalls.includes(node.callee.property.name);
      }
      return htmlCalls.includes(node.callee.name);
    }

    function isDateMethod(node) {
      return dateMethods.includes(node.callee.name);
    }

    function shouldIgnoreCall(node) {
      return (
        node.callee &&
        (isLocalizedCall(node) ||
          isObjectMethod(node) ||
          isHtmlCall(node) ||
          isDateMethod(node))
      );
    }

    function isNewRegExp(node) {
      return node.callee && node.callee.name === "RegExp";
    }

    function canSkipLiteral(node) {
      return (
        isNewRegExp(node.parent) ||
        shouldIgnoreCall(node.parent) ||
        (node.parent.left && isHtmlProperty(node.parent.left)) ||
        (node.parent.id && isHtmlProperty(node.parent.id)) ||
        (node.parent.key && isHtmlProperty(node.parent.key)) ||
        isTrueContstant(node.parent.parent, node.parent) ||
        isKeyList(node.parent.parent, node.parent) ||
        isMemberExpressionIdentifier(node.parent)
      );
    }

    function checkReportAndFix(node) {
      if (canSkipLiteral(node)) return;
      if (isNonLocalizedString(node)) {
        reportAndFix(context, node);
      }
    }

    return {
      Literal(node) {
        checkReportAndFix(node);
      },
      TemplateLiteral(node) {
        checkReportAndFix(node);
      }
    };
  }
};
